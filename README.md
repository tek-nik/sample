# README #

Each branch will contain code used in the articles about spring-swagger-simplified.

### What is this repository for? ###

* Examples to support articles about spring-swagger-simplified

### How do I get set up? ###
* [intro](https://bitbucket.org/tek-nik/sample/src/intro/) Branch contains code for [dzone article](https://dzone.com/articles/simplified-spring-swagger)  explaining how validation constraints can automatically enrich swagger documentation  
* [doingmore](https://bitbucket.org/tek-nik/sample/src/doingmore/) Branch contains code for [dzone article](https://dzone.com/articles/doing-more-with-swaggger-and-spring)  explaining how api info can be displayed , centralized exception reporting can be done.  

